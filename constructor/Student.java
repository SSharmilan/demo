package constructor;

public class Student {
	int id;
	String Name;
	int Age;
	
	Student(int I,String N){
		id=I;
		Name=N;
	}
	
	Student(int I,String N,int A){
		id=I;
		Name=N;
		Age=A;
	}

	
	void display() {
		System.out.println(id+" "+Name+" "+Age);
	}
	
	public static void main(String args[]) {
		
		Student S1=new Student(111,"Sharmilan");
		Student S2=new Student(112,"Suren",21);
		
		S1.display();
		S2.display();
		
	}
}
